// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    type: string = null;
    haveIce: boolean = false;
    curIce: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.type = this.node.name;
        var manager = cc.director.getCollisionManager();
        manager.enabled = true; 
    }

    update (dt) {
        if(this.haveIce){
            if(this.curIce == null) this.haveIce = false;
            else if(this.curIce.x > this.node.x-5 && this.curIce.x < this.node.x+5 &&
                this.curIce.y > this.node.y-5 && this.curIce.y < this.node.y+5){
                    cc.log("Change Direction to " + this.node.name);
                    if(this.node.name == "Left")
                        this.curIce.getComponent(cc.RigidBody).linearVelocity = cc.v2(-500, 0);
                    if(this.node.name == "Right")
                        this.curIce.getComponent(cc.RigidBody).linearVelocity = cc.v2(500, 0);
                    if(this.node.name == "Up")
                        this.curIce.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 500);
                    if(this.node.name == "Down")
                        this.curIce.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                    this.haveIce = false;
            }else if(this.curIce.x > this.node.x+32 || this.curIce.x <this.node.x-32 ||
                this.curIce.y > this.node.y+32 || this.curIce.y < this.node.y-32){
                    this.haveIce = false;
            }
        }
    }

    onBeginContact(contact, self, other){
        if(other.node.name == "Ice" || other.node.name == "Gold Ice"){
            this.haveIce = true;
            this.curIce = other.node;
            contact.disabled = true;
        }
    }
}
