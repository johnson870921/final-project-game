// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class MainCharacter extends cc.Component {

    // @property(cc.Label)
    // label: cc.Label = null;

    // @property
    // text: string = 'hello';

    @property
    anim: cc.Animation;

    
    moveFlag: boolean = false;
    direction: number = 0;
    speed: number = 0;
    initCount: number = 0;
    lastKeycode: any;

    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getPhysicsManager().gravity = cc.v2();
        //initialize property
        this.moveFlag = false;
        this.direction = 1;
        this.speed = 5;

        // //regist the character image
        this.anim = this.getComponent(cc.Animation);
        
        //create collision manager
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;    
        
        //regist keyboard event
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }
    
    start () {
        //this.anim.play('anim_init');
        
    }

    update (dt) {
        if(this.moveFlag == true){
            //this.anim.resume();
            if(this.direction == 0){
                //go up
                //this.node.y += this.speed;
                this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0,140);
            }
            else if(this.direction == 1){
                //go down
                //this.node.y -= this.speed;
                this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0,-140);
            }
            else if(this.direction == 2){
                //go left
                //this.node.x -= this.speed;
                this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(-140,0);
            }
            else if(this.direction == 3){
                //go right
                //this.node.x += this.speed;
                this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(140,0);
                
            }
            
        }
    }

    onKeyDown(event) {
        if(this.moveFlag == false || this.lastKeycode != event.keyCode){
            //cc.log('here');
            this.lastKeycode = event.keyCode;
            if(this.getComponent(cc.Animation).getAnimationState('init').isPlaying){
                this.anim.stop('init');
                
            }
            if(event.keyCode == cc.macro.KEY.up){
                this.direction = 0;
                this.moveFlag = true;
                
                //this.anim.play('back');
                
                this.anim.play('back');
                //this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0,140);
            }
            else if(event.keyCode == cc.macro.KEY.down){
                this.direction = 1;
                this.moveFlag = true;
                
                this.anim.play('foreward');
                
                //this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0,-140);
            }
            else if(event.keyCode == cc.macro.KEY.left){
                this.direction = 2;
                this.moveFlag = true;
                
                this.anim.play('left');
                //this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(-140,0);
            }
            else if(event.keyCode == cc.macro.KEY.right){
                this.direction = 3;
                this.moveFlag = true;
                
                this.anim.play('right');
                //this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(140,0);
            }
        }
        
    }

    onKeyUp(event) {
        if((event.keyCode == cc.macro.KEY.up && this.direction == 0) 
            || (event.keyCode == cc.macro.KEY.down && this.direction == 1)
            || (event.keyCode == cc.macro.KEY.left && this.direction == 2)
            || (event.keyCode == cc.macro.KEY.right && this.direction == 3)){
                this.moveFlag = false;
                
                this.anim.pause();
                this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2();
            }
    }

    onBeginContact(contact, self, other){
        cc.log("Main Character collide: "+other.node.name);
    }
}
