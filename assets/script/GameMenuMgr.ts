// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class GameMenuMgr extends cc.Component {

    @property(cc.AudioClip)
    Bgm: cc.AudioClip = null;

    @property(cc.Node)
    Btn: cc.Node = null;

    @property(cc.Node)
    MusicBtn: cc.Node = null;

    @property(cc.SpriteFrame)
    MusicBtnSprite1: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    MusicBtnSprite2: cc.SpriteFrame = null;

    @property(cc.Node)
    Maps: cc.Node = null;

    @property(cc.Node)
    RuleWindow: cc.Node = null;

    @property(cc.Node)
    OptionWindow: cc.Node = null;

    @property(cc.Node)
    Transition: cc.Node = null;

    @property(cc.Label)
    GreetingLabel: cc.Label = null;

    @property(cc.Label)
    NameLabel: cc.Label = null;

    Name:string = "";

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        if(authorName == "Anounmous"){
            this.CreateRandomGuestName();
        }
        else{
            this.Name = authorName;
        }
        
        this.RuleWindow.active = false;
        this.OptionWindow.active = false;
        this.GreetingLabel.string = "Hi ~ " + this.Name;
    }

    start () {
        if(!cc.audioEngine.isMusicPlaying()){
            cc.audioEngine.playMusic(this.Bgm, true);
        }
        
        authorName = this.Name.toString();
    }

    CreateRandomGuestName(){
        this.Name = "Guest" + Math.floor(Math.random() * 9999).toString();
    }
    // update (dt) {}

    GoToCreateMap() {
        cc.director.loadScene("Create Mode");
    }

    //Go to local stage

    GoToStage1() {
        this.Transition.runAction(cc.moveBy(1, -1000, 0));
        this.scheduleOnce(() => {
            cc.director.loadScene("Stage1");
        }, 1)
    }

    GoToStage2() {
        this.Transition.runAction(cc.moveBy(1, -1000, 0));
        this.scheduleOnce(() => {
            cc.director.loadScene("Stage2");
        }, 1)
    }

    GoToStage3() {
        this.Transition.runAction(cc.moveBy(1, -1000, 0));
        this.scheduleOnce(() => {
            cc.director.loadScene("Stage3");
        }, 1)
    }

    GoToOnlineGame() {
        this.Transition.runAction(cc.moveBy(1, -1000, 0));
        this.scheduleOnce(() => {
            cc.director.loadScene("Online Game");
        }, 1)
    }

    moveBtn() {
        let moveleft = cc.moveBy(0.5, -720, 0).easing(cc.easeInOut(1.5));
        let moveright = cc.moveBy(0.1, 30, 0);
        this.Btn.runAction(cc.sequence(moveleft, moveright));
    }

    MuteorPlayMusic() {
        if(this.MusicBtn.getComponent(cc.Sprite).spriteFrame == this.MusicBtnSprite1) {
            this.MusicBtn.getComponent(cc.Sprite).spriteFrame = this.MusicBtnSprite2;
            cc.audioEngine.pauseMusic();
        }
        else {
            this.MusicBtn.getComponent(cc.Sprite).spriteFrame = this.MusicBtnSprite1;
            cc.audioEngine.resumeMusic();
        }
    }

    moveBack() {
        let moveright = cc.moveBy(0.5, 720, 0).easing(cc.easeInOut(1.5));
        let moveleft = cc.moveBy(0.1, -30, 0);
        this.Btn.runAction(cc.sequence(moveright, moveleft));
    }

    MoveToChooseMap() {
        let moveleft = cc.moveBy(0.5, -940, 0).easing(cc.easeInOut(1.5));
        let moveright = cc.moveBy(0.1, 30, 0);
        this.Btn.runAction(cc.sequence(moveleft, moveright));
    }

    BackToChooseMode() {
        let moveright = cc.moveBy(0.5, 940, 0).easing(cc.easeInOut(1.5));
        let moveleft = cc.moveBy(0.1, -30, 0);
        this.Btn.runAction(cc.sequence(moveright, moveleft));
    }

    MoveLeftMapWindow() {
        if(this.Maps.x <= -500 && this.Maps.x >= -501) {
            let moveleft = cc.moveBy(0.1, -30, 0).easing(cc.easeInOut(1.5));
            let moveright = cc.moveBy(0.1, 30, 0);
            this.Maps.runAction(cc.sequence(moveleft, moveright));
        }
        else if(this.Maps.x >= -1) {
            let moveleft = cc.moveBy(0.5, -530, 0).easing(cc.easeInOut(1.5));
            let moveright = cc.moveBy(0.1, 30, 0);
            this.Maps.runAction(cc.sequence(moveleft, moveright));
        }
    }

    MoveRightMapWindow() {
        if(this.Maps.x >= -1 && this.Maps.x <= 1) {
            let moveright = cc.moveBy(0.1, 30, 0).easing(cc.easeInOut(1.5));
            let moveleft = cc.moveBy(0.1, -30, 0);
            this.Maps.runAction(cc.sequence(moveright, moveleft));
            this.Maps.x = 0;
        }
        else if(this.Maps.x <= -400) {
            let moveright = cc.moveBy(0.5, 530, 0).easing(cc.easeInOut(1.5));
            let moveleft = cc.moveBy(0.1, -30, 0);
            this.Maps.runAction(cc.sequence(moveright, moveleft));
        }
    }

    ShowRule() {
        this.RuleWindow.active = true;
    }

    HideRule() {
        this.RuleWindow.active = false;
    }

    ShowOption() {
        this.OptionWindow.active = true;
    }

    HideOption() {
        this.OptionWindow.active = false;
    }

    ChangeName() {
        if(this.NameLabel.string != "") {
            this.Name = this.NameLabel.string;
            authorName = this.Name.toString();
            this.GreetingLabel.string = "Hi ~ " + this.Name.toString();
            this.OptionWindow.active = false;
        }
    }

}
