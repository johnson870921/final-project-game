// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
import GameManager from "./GameManager";
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Prefab)
    ice: cc.Prefab = null;
    @property(cc.Prefab)
    flame: cc.Prefab = null;
    @property(cc.Prefab)
    obstacle: cc.Prefab = null;
    @property(cc.Prefab)
    character: cc.Prefab = null;
    @property(cc.Prefab)
    unlimitedGoldenIce: cc.Prefab = null;
    @property(cc.Prefab)
    unlimitedIce: cc.Prefab = null;
    @property(cc.Prefab)
    goldenIce: cc.Prefab = null;
    @property(cc.Prefab)
    up: cc.Prefab = null;
    @property(cc.Prefab)
    down: cc.Prefab = null;
    @property(cc.Prefab)
    left: cc.Prefab = null;
    @property(cc.Prefab)
    right: cc.Prefab = null;

    @property(cc.SpriteFrame)
    bgmDisabled: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    bgmAbled: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    soundDisabled: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    soundAbled: cc.SpriteFrame = null;

    soundFlag: boolean = true;
    background: cc.Node = null;
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.background = this.node.getChildByName("Background");
        //intial physics manager
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getPhysicsManager().gravity = cc.v2();
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("HomeBtn").on('click', this.returnToMenu, this);
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("RestartBtn").on('click', this.returnToCreateMode, this);
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("Name").getChildByName("New Label").getComponent(cc.Label)
            .string = authorName;
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("MusicBtn").on('click', this.toggleMusic, this);
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("SoundBtn").on('click', this.toggleSound, this);
    }

    start () {
        this.UpdateAllPrefabFromLocalStorage();
    }

    // update (dt) {}

    UpdateAllPrefabFromLocalStorage(){
        //ice
        for(var i=0; i<iceListX.length; i++){
            var node = cc.instantiate(this.ice);
            node.x = iceListX[i];
            node.y = iceListY[i];
            node.parent = this.background;
        }
        //flame
        flameCount = flameListX.length;
        for(var i=0; i<flameListX.length; i++){
            var node = cc.instantiate(this.flame);
            node.x = flameListX[i];
            node.y = flameListY[i];
            node.parent = this.background;
        }
        //unlimited golden ice
        for(var i=0; i<unlimitedGoldenIceX.length; i++){
            var node = cc.instantiate(this.unlimitedGoldenIce);
            node.x = unlimitedGoldenIceX[i];
            node.y = unlimitedGoldenIceY[i];
            node.parent = this.background;
        }
        //obstacle
        for(var i=0; i<obstacleListX.length; i++){
            var node = cc.instantiate(this.obstacle);
            node.x = obstacleListX[i];
            node.y = obstacleListY[i];
            node.parent = this.background;
        }
        //unlimited ice
        for(var i=0; i<unlimitedIceX.length; i++){
            var node = cc.instantiate(this.unlimitedIce);
            node.x = unlimitedIceX[i];
            node.y = unlimitedIceY[i];
            node.parent = this.background;
        }
        //golden ice
        for(var i=0; i<goldenIceX.length; i++){
            var node = cc.instantiate(this.goldenIce);
            node.x = goldenIceX[i];
            node.y = goldenIceY[i];
            node.parent = this.background;
        }
        //up
        for(var i=0; i<upX.length; i++){
            var node = cc.instantiate(this.up);
            node.x = upX[i];
            node.y = upY[i];
            node.parent = this.background;
        }
        //down
        for(var i=0; i<downX.length; i++){
            var node = cc.instantiate(this.down);
            node.x = downX[i];
            node.y = downY[i];
            node.parent = this.background;
        }
        //left
        for(var i=0; i<leftX.length; i++){
            var node = cc.instantiate(this.left);
            node.x = leftX[i];
            node.y = leftY[i];
            node.parent = this.background;
        }
        //right
        for(var i=0; i<rightX.length; i++){
            var node = cc.instantiate(this.right);
            node.x = rightX[i];
            node.y = rightY[i];
            node.parent = this.background;
        }
        //character
        var node = cc.instantiate(this.character);
        node.x = characterX;
        node.y = characterY;
        node.parent = this.background;
    }

    returnToMenu(){
        cc.director.loadScene("Menu");
    }

    returnToCreateMode(){
        cc.director.loadScene(cc.director.getScene().name);
    }

    toggleMusic(){
        
        if(cc.audioEngine.isMusicPlaying()){
            this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("MusicBtn")
            .getComponent(cc.Sprite).spriteFrame = this.bgmDisabled;
            cc.audioEngine.pauseMusic();
        }
        else{
            this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("MusicBtn")
            .getComponent(cc.Sprite).spriteFrame = this.bgmAbled;
            cc.audioEngine.resumeMusic();
        }
    }
    toggleSound(){
        if(!this.soundFlag){
            this.soundFlag = true;
            this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("SoundBtn")
            .getComponent(cc.Sprite).spriteFrame = this.soundAbled;
            //cc.audioEngine.stopAllEffects();
            this.node.getChildByName("Background").getComponent(GameManager).soundEffectFlag = true;
        }
        else{
            this.soundFlag = false;
            this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("SoundBtn")
            .getComponent(cc.Sprite).spriteFrame = this.soundDisabled;
            //cc.audioEngine.resumeAllEffects();
            this.node.getChildByName("Background").getComponent(GameManager).soundEffectFlag = false;
        }
    }
}
