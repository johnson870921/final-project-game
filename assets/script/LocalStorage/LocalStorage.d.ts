declare let characterX: number;
declare let characterY: number;

declare let flameListX: Array<number>;
declare let flameListY: Array<number>;

declare let obstacleListX: Array<number>;
declare let obstacleListY: Array<number>;

declare let iceListX: Array<number>;
declare let iceListY: Array<number>;

declare let unlimitedGoldenIceX: Array<number>;
declare let unlimitedGoldenIceY: Array<number>;

declare let unlimitedIceX: Array<number>;
declare let unlimitedIceY: Array<number>;

declare let goldenIceX: Array<number>;
declare let goldenIceY: Array<number>;

declare let upX: Array<number>;
declare let upY: Array<number>;

declare let downX: Array<number>;
declare let downY: Array<number>;

declare let leftX: Array<number>;
declare let leftY: Array<number>;

declare let rightX: Array<number>;
declare let rightY: Array<number>;

declare let selectOnlineStage: number;
declare let authorName: string;
declare let flameCount: number;

declare let CurrentStage: number;
// declare let pushFlame: function;