// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
import GameManager from "./GameManager";
const {ccclass, property} = cc._decorator;

@ccclass
export default class OnlineGameMgr extends cc.Component {

    @property(cc.Prefab)
    ice: cc.Prefab = null;
    @property(cc.Prefab)
    flame: cc.Prefab = null;
    @property(cc.Prefab)
    obstacle: cc.Prefab = null;
    @property(cc.Prefab)
    character: cc.Prefab = null;
    @property(cc.Prefab)
    unlimitedGoldenIce: cc.Prefab = null;
    @property(cc.Prefab)
    unlimitedIce: cc.Prefab = null;
    @property(cc.Prefab)
    goldenIce: cc.Prefab = null;
    @property(cc.Prefab)
    up: cc.Prefab = null;
    @property(cc.Prefab)
    down: cc.Prefab = null;
    @property(cc.Prefab)
    left: cc.Prefab = null;
    @property(cc.Prefab)
    right: cc.Prefab = null;

    @property(cc.SpriteFrame)
    bgmDisabled: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    bgmAbled: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    soundDisabled: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    soundAbled: cc.SpriteFrame = null;

    soundFlag: boolean = true;
    background: cc.Node = null;
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.background = this.node.getChildByName("Background");
        //intial physics manager
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getPhysicsManager().gravity = cc.v2();
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("HomeBtn").on('click', this.returnToMenu, this);
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("RestartBtn").on('click', this.returnToCreateMode, this);
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("left_btn").on('click', this.goToNextStage, this);
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("right_btn").on('click', this.goToPrevStage, this);
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("MusicBtn").on('click', this.toggleMusic, this);
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("SoundBtn").on('click', this.toggleSound, this);
    }

    start () {
        //this.UpdateAllPrefabFromLocalStorage();
       
        this.UpdateAllPrefabFromFirebase();
    }

    // update (dt) {}

    UpdateAllPrefabFromFirebase(){
        var db = firebase.database();
        this.resetLocalStorage();
        var pAuthor = db.ref('/'+selectOnlineStage+'/author').once('value', snapshot =>{
            var author = snapshot.val();
            //cc.log("author: "+author);
            this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("Name").getChildByName("New Label").getComponent(cc.Label)
            .string = author;
        })
        var pFlameX = db.ref('/'+selectOnlineStage+'/flameListX').once('value', function(snapshot){
            snapshot.forEach(element => {
                flameListX.push(element.val().flameListX);
            });      
        })
        var pFlameY = db.ref('/'+selectOnlineStage+'/flameListY').once('value', function(snapshot){
            snapshot.forEach(element => {
                flameListY.push(element.val().flameListY);
            });      
        })
        var pObstacleListX = db.ref('/'+selectOnlineStage+'/obstacleListX').once('value', function(snapshot){
            snapshot.forEach(element => {
                obstacleListX.push(element.val().obstacleListX);
            });      
        })
        var pObstacleListY = db.ref('/'+selectOnlineStage+'/obstacleListY').once('value', function(snapshot){
            snapshot.forEach(element => {
                obstacleListY.push(element.val().obstacleListY);
            });      
        })
        var pCharacterX = db.ref('/'+selectOnlineStage+'/characterX').once('value', function(snapshot){
            characterX = snapshot.val();
                 
        })
        var pCharacterY = db.ref('/'+selectOnlineStage+'/characterY').once('value', function(snapshot){
            characterY = snapshot.val();      
        })
        var pIceListX = db.ref('/'+selectOnlineStage+'/iceListX').once('value', function(snapshot){
            snapshot.forEach(element => {
                iceListX.push(element.val().iceListX);
            });      
        })
        var pIceListY = db.ref('/'+selectOnlineStage+'/iceListY').once('value', function(snapshot){
            snapshot.forEach(element => {
                iceListY.push(element.val().iceListY);
            });      
        })
        var pUnlimitedGoldenIceX = db.ref('/'+selectOnlineStage+'/unlimitedGoldenIceX').once('value', function(snapshot){
            snapshot.forEach(element => {
                unlimitedGoldenIceX.push(element.val().unlimitedGoldenIceX);
            });      
        })
        var pUnlimitedGoldenIceY = db.ref('/'+selectOnlineStage+'/unlimitedGoldenIceY').once('value', function(snapshot){
            snapshot.forEach(element => {
                unlimitedGoldenIceY.push(element.val().unlimitedGoldenIceY);
            });      
        })
        var pUnlimitedIceX = db.ref('/'+selectOnlineStage+'/unlimitedIceX').once('value', function(snapshot){
            snapshot.forEach(element => {
                unlimitedIceX.push(element.val().unlimitedIceX);
            });      
        })
        var pUnlimitedIceY = db.ref('/'+selectOnlineStage+'/unlimitedIceY').once('value', function(snapshot){
            snapshot.forEach(element => {
                unlimitedIceY.push(element.val().unlimitedIceY);
            });      
        })
        var pGoldenIceX = db.ref('/'+selectOnlineStage+'/goldenIceX').once('value', function(snapshot){
            snapshot.forEach(element => {
                goldenIceX.push(element.val().goldenIceX);
            });      
        })
        var pGoldenIceY = db.ref('/'+selectOnlineStage+'/goldenIceY').once('value', function(snapshot){
            snapshot.forEach(element => {
                goldenIceY.push(element.val().goldenIceY);
            });      
        })
        var pUpX = db.ref('/'+selectOnlineStage+'/upX').once('value', function(snapshot){
            snapshot.forEach(element => {
                upX.push(element.val().upX);
            });      
        })
        var pUpY = db.ref('/'+selectOnlineStage+'/upY').once('value', function(snapshot){
            snapshot.forEach(element => {
                upY.push(element.val().upY);
            });      
        })
        var pDownX = db.ref('/'+selectOnlineStage+'/downX').once('value', function(snapshot){
            snapshot.forEach(element => {
                downX.push(element.val().downX);
            });      
        })
        var pDownY = db.ref('/'+selectOnlineStage+'/downY').once('value', function(snapshot){
            snapshot.forEach(element => {
                downY.push(element.val().downY);
            });      
        })
        var pLeftX = db.ref('/'+selectOnlineStage+'/leftX').once('value', function(snapshot){
            snapshot.forEach(element => {
                leftX.push(element.val().leftX);
            });      
        })
        var pLeftY = db.ref('/'+selectOnlineStage+'/leftY').once('value', function(snapshot){
            snapshot.forEach(element => {
                leftY.push(element.val().leftY);
            });      
        })
        var pRightX = db.ref('/'+selectOnlineStage+'/rightX').once('value', function(snapshot){
            snapshot.forEach(element => {
                rightX.push(element.val().rightX);
            });      
        })
        var pRightY = db.ref('/'+selectOnlineStage+'/rightY').once('value', function(snapshot){
            snapshot.forEach(element => {
                rightY.push(element.val().rightY);
            });      
        })
        Promise.all([pFlameX, pFlameY, pObstacleListX, pObstacleListY, pCharacterX, pCharacterY,
            pIceListX, pIceListY, pUnlimitedGoldenIceX, pUnlimitedGoldenIceY, pUnlimitedIceX, pUnlimitedIceY,
            pGoldenIceX, pGoldenIceY, pUpX, pUpY, pDownX, pDownY, pRightX, pRightY, pLeftX, pLeftY])
            .then(e => {
                //ice
                for(var i=0; i<iceListX.length; i++){
                    var node = cc.instantiate(this.ice);
                    node.x = iceListX[i];
                    node.y = iceListY[i];
                    node.parent = this.background;
                }
                //flame
                flameCount = flameListX.length;
                for(var i=0; i<flameListX.length; i++){
                    var node = cc.instantiate(this.flame);
                    node.x = flameListX[i];
                    node.y = flameListY[i];
                    node.parent = this.background;
                }
                //unlimited golden ice
                for(var i=0; i<unlimitedGoldenIceX.length; i++){
                    var node = cc.instantiate(this.unlimitedGoldenIce);
                    node.x = unlimitedGoldenIceX[i];
                    node.y = unlimitedGoldenIceY[i];
                    node.parent = this.background;
                }
                //obstacle
                for(var i=0; i<obstacleListX.length; i++){
                    var node = cc.instantiate(this.obstacle);
                    node.x = obstacleListX[i];
                    node.y = obstacleListY[i];
                    node.parent = this.background;
                }
                //unlimited ice
                for(var i=0; i<unlimitedIceX.length; i++){
                    var node = cc.instantiate(this.unlimitedIce);
                    node.x = unlimitedIceX[i];
                    node.y = unlimitedIceY[i];
                    node.parent = this.background;
                }
                //golden ice
                for(var i=0; i<goldenIceX.length; i++){
                    var node = cc.instantiate(this.goldenIce);
                    node.x = goldenIceX[i];
                    node.y = goldenIceY[i];
                    node.parent = this.background;
                }
                //up
                for(var i=0; i<upX.length; i++){
                    var node = cc.instantiate(this.up);
                    node.x = upX[i];
                    node.y = upY[i];
                    node.parent = this.background;
                }
                //down
                for(var i=0; i<downX.length; i++){
                    var node = cc.instantiate(this.down);
                    node.x = downX[i];
                    node.y = downY[i];
                    node.parent = this.background;
                }
                //left
                for(var i=0; i<leftX.length; i++){
                    var node = cc.instantiate(this.left);
                    node.x = leftX[i];
                    node.y = leftY[i];
                    node.parent = this.background;
                }
                //right
                for(var i=0; i<rightX.length; i++){
                    var node = cc.instantiate(this.right);
                    node.x = rightX[i];
                    node.y = rightY[i];
                    node.parent = this.background;
                }
                //character
                var node = cc.instantiate(this.character);
                node.x = characterX;
                node.y = characterY;
                node.parent = this.background;
            })
        
    }

    returnToMenu(){
        cc.director.loadScene("Menu");
    }

    returnToCreateMode(){
        cc.director.loadScene("Online Game");
    }

    resetLocalStorage(){
        characterX = -64;
        characterY = 0;
        flameListX = [];
        flameListY = [];
        obstacleListX = [];
        obstacleListY = [];
        iceListX = [];
        iceListY = [];
        unlimitedGoldenIceX = [];
        unlimitedGoldenIceY = [];
        unlimitedIceX = [];
        unlimitedIceY = [];
        goldenIceX = [];
        goldenIceY = [];
        upX = [];
        upY = [];
        downX = [];
        downY = [];
        leftX = [];
        leftY = [];
        rightX = [];
        rightY = [];
        
    }

    goToNextStage(){
        var stageNum = 0;
        var totalStage = 0;
        var db = firebase.database();
        db.ref('totalStage').once('value', function(snapshot){
            totalStage = snapshot.val();
            //cc.log("totalStage " + totalStage);
        }).then(e=>{
            selectOnlineStage++;
            if(selectOnlineStage%totalStage == 0){
                stageNum = totalStage;
            }
            else{
                stageNum = selectOnlineStage%totalStage;
            }
            //stageNum = selectOnlineStage%(totalStage)+1;
            selectOnlineStage = stageNum;
            cc.director.loadScene("Online Game");
        })
    }

    goToPrevStage(){
        var stageNum = 0;
        var totalStage = 0;
        var db = firebase.database();
        db.ref('totalStage').once('value', function(snapshot){
            totalStage = snapshot.val();
            //cc.log("totalStage " + totalStage);
        }).then(e=>{
            selectOnlineStage--;       
            if(selectOnlineStage%totalStage == 0){
                stageNum = totalStage;
            }
            else{
                stageNum = selectOnlineStage%totalStage;
            }
            selectOnlineStage = stageNum;
            cc.director.loadScene("Online Game");
        })
    }

    toggleMusic(){
        if(cc.audioEngine.isMusicPlaying()){
            this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("MusicBtn")
            .getComponent(cc.Sprite).spriteFrame = this.bgmDisabled;
            cc.audioEngine.pauseMusic();
        }
        else{
            this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("MusicBtn")
            .getComponent(cc.Sprite).spriteFrame = this.bgmAbled;
            cc.audioEngine.resumeMusic();
        }
    }
    toggleSound(){
        if(!this.soundFlag){
            this.soundFlag = true;
            this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("SoundBtn")
            .getComponent(cc.Sprite).spriteFrame = this.soundAbled;
            this.node.getChildByName("Background").getComponent(GameManager).soundEffectFlag = true;
            //cc.audioEngine.stopAllEffects();
        }
        else{
            this.soundFlag = false;
            this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("SoundBtn")
            .getComponent(cc.Sprite).spriteFrame = this.soundDisabled;
            this.node.getChildByName("Background").getComponent(GameManager).soundEffectFlag = false;
            //cc.audioEngine.resumeAllEffects();
        }
    }
}
