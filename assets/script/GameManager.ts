// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    //@property(cc.Label)
    @property(cc.SpriteFrame)
    bgmBtnAbled: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    bgmBtnDisabled: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    effectBtnAbled: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    effectBtnDisabled: cc.SpriteFrame = null;
    @property(cc.Node)
    Scoreboard: cc.Node = null;
    @property(cc.RichText)
    Score: cc.RichText = null;
    @property(cc.Node)
    Congratulations: cc.Node = null;

    timeText: cc.Label = null;
    movementText: cc.Label = null;
    
    
    private time: number = 0;
    private movement: number = 0;

    soundEffectFlag: boolean = true;
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        if(cc.director.getScene().name == "Stage1") {
            CurrentStage = 1;
        }
        else if(cc.director.getScene().name == "Stage2") {
            CurrentStage = 2;
        }
        else if(cc.director.getScene().name == "Stage3") {
            CurrentStage = 3;
        }
        else if(cc.director.getScene().name == "Menu") {
            CurrentStage = 0;
        }
        else if(cc.director.getScene().name == "Local Game") {
            CurrentStage = -1;
        }
        else if(cc.director.getScene().name == "Online Game") {
            CurrentStage = -2;
        }
        //this.node.getChildByName("toolsbar").getChildByName("MusicBtn").on('click',this.toggleBGM, this);
        //this.node.getChildByName("toolsbar").getChildByName("SoundBtn").on('click',this.toggleEffect, this);
        //this.node.getChildByName("toolsbar").getChildByName("RestartBtn").on('click',this.resetTheStage, this);
        //this.node.getChildByName("toolsbar").getChildByName("HomeBtn").on('click',this.gotoMenu, this);
    }

    start () {
        this.timeText = cc.find("Canvas/Background/toolsbar/Time/New Label").getComponent(cc.Label);
        this.timeText.string = this.time.toString();
        this.movementText = cc.find("Canvas/Background/toolsbar/Movement/New Label").getComponent(cc.Label);
        this.movementText.string = this.time.toString();
        this.schedule(function() {
            this.time += 1;
            this.timeText.string = this.time.toString();
            //cc.log("Used Time: " + this.usedTime); 
            //cc.log("Push Time: " + this.pushTime); 
        }, 1);
    }

    // update (dt) {}

    addMovement(){
        this.movement++;
        this.movementText.string = this.movement.toString();
    }

    checkIfAllFlameDestory(){
        var flag = this.node.getChildByName("Flame");
        if(flag == null){
            //clear stage
            cc.log('clear stage');
        }
    }

    gotoMenu(){
        cc.director.loadScene("Menu");
    }
    resetTheStage(){
        cc.director.loadScene(cc.director.getScene().name);
    }
    toggleBGM(){
        if(cc.audioEngine.isMusicPlaying){
            cc.audioEngine.pauseMusic();
            //this.node.getChildByName("toolsbar").getChildByName("MusicBtn").getComponent(cc.Sprite).spriteFrame = this.bgmBtnDisabled;
        }
        else{
            cc.audioEngine.resumeMusic();
            //this.node.getChildByName("toolsbar").getChildByName("MusicBtn").getComponent(cc.Sprite).spriteFrame = this.bgmBtnAbled;
        }
    }

    toggleEffect(){
        if(this.soundEffectFlag){
            this.soundEffectFlag = false;
            cc.log('here');
            //cc.audioEngine.stopAllEffects();
            //this.node.getChildByName("toolsbar").getChildByName("SoundBtn").getComponent(cc.Sprite).spriteFrame = this.effectBtnDisabled;
        }
        else{
            //cc.audioEngine.resumeAllEffects();
            cc.log('here');
            this.soundEffectFlag = true;
            //this.node.getChildByName("toolsbar").getChildByName("SoundBtn").getComponent(cc.Sprite).spriteFrame = this.effectBtnAbled;
        }
    }

    moveScoreboard() {
        let score = Math.round(1/(this.time + this.movement*4)*5000);
        this.Score.string = score.toString();
        this.Scoreboard.runAction(cc.moveBy(1, 0, 525));
        if(CurrentStage == 1) {
            this.scheduleOnce(() => {
                cc.director.loadScene("Stage2");
            }, 6)
        }
        else if(CurrentStage == 2) {
            this.scheduleOnce(() => {
                cc.director.loadScene("Stage3");
            }, 6)
        }
        else if(CurrentStage == 3) {
            this.scheduleOnce(() => {
                cc.director.loadScene("Menu");
            }, 6)
        }
        else if(CurrentStage == -1) {
            this.scheduleOnce(() => {
                cc.director.loadScene("Menu");
            }, 6)
        }

    }
}
