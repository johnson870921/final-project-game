// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
import GameManager from "./GameManager";
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.SpriteFrame)
    bgmDisabled: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    bgmAbled: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    soundDisabled: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    soundAbled: cc.SpriteFrame = null;
    @property
    totalFlame: number = 0;
    soundFlag: boolean = true;
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("MusicBtn").on('click', this.toggleMusic, this);
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("SoundBtn").on('click', this.toggleSound, this);
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("HomeBtn").on('click', this.returnToMenu, this);
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("RestartBtn").on('click', this.returnToCreateMode, this);
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("Name").children[0].getComponent(cc.Label).string
        = authorName;
        flameCount = this.totalFlame;
    }

    start () {

    }

    // update (dt) {}

    toggleMusic(){
        if(cc.audioEngine.isMusicPlaying()){
            this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("MusicBtn")
            .getComponent(cc.Sprite).spriteFrame = this.bgmDisabled;
            cc.audioEngine.pauseMusic();
        }
        else{
            this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("MusicBtn")
            .getComponent(cc.Sprite).spriteFrame = this.bgmAbled;
            cc.audioEngine.resumeMusic();
        }
    }
    toggleSound(){
        if(!this.soundFlag){
            this.soundFlag = true;
            this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("SoundBtn")
            .getComponent(cc.Sprite).spriteFrame = this.soundAbled;
            this.node.getChildByName("Background").getComponent(GameManager).soundEffectFlag = true;
            //cc.audioEngine.stopAllEffects();
        }
        else{
            this.soundFlag = false;
            this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("SoundBtn")
            .getComponent(cc.Sprite).spriteFrame = this.soundDisabled;
            this.node.getChildByName("Background").getComponent(GameManager).soundEffectFlag = false;
            //cc.audioEngine.resumeAllEffects();
        }
    }

    returnToMenu(){
        cc.director.loadScene("Menu");
    }

    returnToCreateMode(){
        cc.director.loadScene(cc.director.getScene().name);
    }
}
