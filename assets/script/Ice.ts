// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import GameManager from "./GameManager";

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property({ type: cc.AudioClip })
    pushEffect: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    destroyEffect: cc.AudioClip = null;

    direction: number = 0;//0 up, 1 down, 2 left, 3 right
    moveFlag: boolean = false;
    speed: number = 10;
    
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        //create collision manager
        var manager = cc.director.getCollisionManager();
        manager.enabled = true; 
        //manager.enabledDebugDraw = true;
        //this.collider.node.on(cc.Node.EventType.TOUCH_START, this.onCollision);
    }

    start () {
        
    }

    update (dt) {
        if(this.moveFlag){
            if(this.direction == 0){
                //this.node.y += this.speed;
            }
            else if(this.direction == 1){
                //this.node.y -= this.speed;
            }
            else if(this.direction == 2){
                //this.node.x -= this.speed;
            }
            else if(this.direction == 3){
                //this.node.x += this.speed;
            }
        }
    }

    onCollisionEnter(other, self){
        cc.log('Ice collide');
        var otherNodeName = other.node.name;
        //if ice is been pushed
        //other coordinate
        
        var otherX = other.node.x;
        var otherY = other.node.y;
        var posX = this.node.x;
        var posY = this.node.y;
        // cc.log("Collide posX: " + other.node.x);
        // cc.log("Collide posY: " + other.node.y);
        // cc.log("x: " + this.node.x);
        // cc.log("y: " + this.node.y);
        if(otherNodeName == 'Main Character'){
            if(posX + 16 < otherX){
                cc.log('right');
                this.moveFlag = true;
                this.direction = 2;
                
            }
            else if(posX - 16 > otherX ){
                cc.log('left');
                this.moveFlag = true;
                this.direction = 3;
                
            }
            else if(posY + 16 < otherY){
                cc.log('up');
                this.moveFlag = true;
                this.direction = 1;
                
            }
            else if(posY - 16 > otherY ){
                cc.log('down');
                this.moveFlag = true;
                this.direction = 0;
                
            }
            
        }
    }

    onBeginContact(contact, self, other){
        //cc.log(other.node.name);
        var worldManifold = contact.getWorldManifold().normal;
        if(other.node.name == 'Main Character'){
            //cc.log('Character collide');
            //cc.log(self.node.name);
            if(!this.moveFlag){
                if(worldManifold.y==1 || other.direction == 1){
                    self.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -500);
                    this.moveFlag = true;
                }else if(worldManifold.y==-1  || other.direction == 0){
                    self.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 500);
                    this.moveFlag = true;
                }else if(worldManifold.x==1 || other.direction == 2){
                    self.getComponent(cc.RigidBody).linearVelocity = cc.v2(-500, 0);
                    this.moveFlag = true;
                }else if(worldManifold.x==-1  || other.direction == 3){
                    self.getComponent(cc.RigidBody).linearVelocity = cc.v2(500, 0);
                    this.moveFlag = true;
                }if(this.moveFlag){
                    cc.find("Canvas/Background").getComponent(GameManager).addMovement();
                    if(this.node.parent.getComponent(GameManager).soundEffectFlag){
                        cc.audioEngine.playEffect(this.pushEffect, false);
                    }
                }
            }else{
                self.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 0);
                this.moveFlag = false;
            }
            /*var points = worldManifold.points;
            var vel1 = cc.v2();
            self.node.getComponent(cc.RigidBody).getLinearVelocityFromWorldPoint(worldManifold.points[0],vel1);
            
            var vel2 = cc.v2();
            other.node.getComponent(cc.RigidBody).getLinearVelocityFromWorldPoint(worldManifold.points[0],vel2);
            
            var relativeVelocity = vel1.sub(vel2);
            //cc.log("relVelo: "+relativeVelocity);
            
            //this.getComponent(cc.RigidBody).applyForceToCenter(new cc.Vec2(1,10000000), true);
            
            this.getComponent(cc.RigidBody).linearVelocity = cc.v2(-2*relativeVelocity.x, -2*relativeVelocity.y);*/
        }else if(other.node.name == 'Flame'){
            this.node.destroy();
            if(this.node.parent.getComponent(GameManager).soundEffectFlag){
                cc.audioEngine.playEffect(this.destroyEffect, false);
            }
            
            flameCount--;
            if(flameCount == 0){
                cc.find("Canvas/Background").getComponent(GameManager).moveScoreboard();
                cc.log("clear stage");
            }
        }else if(other.node.name=="Ice" || other.node.name=="Gold Ice"){
            var originSpeed = self.getComponent(cc.RigidBody).linearVelocity;
            if(worldManifold.y==1 || worldManifold.y==-1)
                self.getComponent(cc.RigidBody).linearVelocity = originSpeed.scale(cc.v2(1, 0));
            else if(worldManifold.x==1 || worldManifold.x==-1)
            self.getComponent(cc.RigidBody).linearVelocity = originSpeed.scale(cc.v2(0, 1));
            this.moveFlag = false;
        }else if(other.node.name!="Left" && other.node.name!="Right" && other.node.name!="Up" && other.node.name!="Down")
            this.moveFlag = false;
    }

    onPostSolve(contact, self, other) {
        var worldManifold = contact.getWorldManifold().normal;
        var originSpeed = self.getComponent(cc.RigidBody).linearVelocity;
        if(other.node.name=="Ice" || other.node.name=="Gold Ice"){
            if(worldManifold.y==1 || worldManifold.y==-1)
                self.getComponent(cc.RigidBody).linearVelocity = originSpeed.scale(cc.v2(1, 0));
            else if(worldManifold.x==1 || worldManifold.x==-1)
            self.getComponent(cc.RigidBody).linearVelocity = originSpeed.scale(cc.v2(0, 1));
        } 
    }
    
}
