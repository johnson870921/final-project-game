// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property(cc.Prefab)
    icePrefab: cc.Prefab = null;

    @property(cc.Node)
    player: cc.Node = null;

    @property(cc.Node)
    curIce: cc.Node = null;

    making: boolean = false;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {}

    start () {
        cc.log("make ice");
        this.curIce = cc.instantiate(this.icePrefab);
        this.curIce.position = this.node.position;
        cc.find("Canvas/Background").addChild(this.curIce);
        this.player = cc.find("Canvas/Background").getChildByName("Main Character");
    }

    update (dt) {
        if((this.curIce.x > this.node.x+30 || this.curIce.y > this.node.y+30 ||
            this.curIce.x < this.node.x-30 || this.curIce.y < this.node.y-30) &&
                (this.player.x > this.node.x+30 || this.player.x < this.node.x-30 ||
                    this.player.y > this.node.y+30 || this.player.y < this.node.y-30)){
                        if(!this.making){
                            this.making = true;
                            cc.log("make ice");
                            this.curIce = cc.instantiate(this.icePrefab);
                            this.curIce.position = this.node.position;
                            this.scheduleOnce(() => {
                                cc.find("Canvas/Background").addChild(this.curIce);
                                this.making = false;
                            }, 1.5);
                        }
            
        }
    }

   
}
