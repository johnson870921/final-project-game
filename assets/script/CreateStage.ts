// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    
    @property(cc.SpriteFrame)
    flame: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    character: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    obstacle: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    ice: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    unlimitedGoldenIce: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    unlimitedIce: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    goldenIce: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    up: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    down: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    left: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    right: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    bgmDisabled: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    bgmAbled: cc.SpriteFrame = null;

    currentSelectObject: number = 2;
    characterCreateFlag: boolean = false;
    currentObjRef: cc.Node = null;
    mouseX: number = 0;
    mouseY: number = 0;
    // flameList: cc.Node[] = [];
    // characterList: cc.Node = null;
    // obstacleList: cc.Node[] = [];
    isDrag: boolean = false;
    questionMark: cc.Node = null;
    board: cc.Node = null;
    // LIFE-CYCLE CALLBACKS:

    onLoad () {

        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        this.node.on(cc.Node.EventType.MOUSE_DOWN, this.onMouseDown, this);
        this.node.on(cc.Node.EventType.MOUSE_UP, this.onMouseUp, this);
        this.node.on(cc.Node.EventType.MOUSE_MOVE, function(event){
            
            //cc.log(event._x);
            this.mouseX = event._x - 480;
            this.mouseY = event._y - 320;
        },this)
        this.board = this.node.getChildByName("Background").getChildByName("GuideBoard");
        this.questionMark = this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("QuestionBtn");
        this.questionMark.on('click', this.toggleBoard, this);
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("StartBtn").on('click', this.startLocalGame, this);
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("UploadBtn").on('click',function(){
            this.saveLastObjToLocalStorage();
            this.uploadToFirebase();
        }, this);
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("ResetBtn").on('click', function(){
            this.resetLocalStorage();
            cc.director.loadScene("Create Mode");
        }, this);
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("HomeBtn").on('click', this.returnToMenu, this);
        this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("MusicBtn").on('click', this.toggleMusic, this);
    }

    start () {
        var graph = this.node.getChildByName('Background').getChildByName('Layout').getComponent(cc.Graphics);
        for(var i=0; i<30; i++){
            graph.moveTo(-480+i*32, -320);
            graph.lineTo(-480+i*32, 320);
            graph.stroke();
        }
        for(var i=0; i<20; i++){
            graph.moveTo(-480,-320+i*32);
            graph.lineTo(480,-320+i*32);
            graph.stroke();
        }
        this.RecoverAllSpriteFrameFromLocalStorage();
    }

    update (dt) {
        if(this.isDrag && this.mouseX < 270 && this.mouseX > -430 && this.mouseY < 272 && this.mouseY > -272){
            this.currentObjRef.x = this.mouseX;
            this.currentObjRef.y = this.mouseY;
        }

    }

    onKeyDown(event) {
        //save last obj to global
        this.saveLastObjToLocalStorage();
        // for(var i=0; i<flameListX.length; i++){
        //     cc.log(i + " x: " + flameListX[i]);
        //     cc.log(i + " y: " + flameListY[i]);
        // }
        

        switch(event.keyCode) {
            case cc.macro.KEY.a:
                //cc.log('Flame');
                this.currentSelectObject = 1;      
                break;
            case cc.macro.KEY.s:
                //cc.log('Character');
                this.currentSelectObject = 2;     
                break;
            case cc.macro.KEY.d:
                //cc.log('Obstacle');
                this.currentSelectObject = 3;
                break;

            case cc.macro.KEY.f:
                //cc.log('Ice');
                this.currentSelectObject = 4;
                break;
            case cc.macro.KEY.z:
                //cc.log('up');
                this.currentSelectObject = 5;
                break;
            case cc.macro.KEY.x:
                // cc.log('down');
                this.currentSelectObject = 6;
                break;
            case cc.macro.KEY.c:
                // cc.log('left');
                this.currentSelectObject = 7;
                break;
            case cc.macro.KEY.v:
                // cc.log('right');
                this.currentSelectObject = 8;
                break;
            case cc.macro.KEY.q:
                // cc.log('unlimited golden ice');
                this.currentSelectObject = 9;
                break;
            case cc.macro.KEY.w:
                // cc.log('unlimited ice');
                this.currentSelectObject = 10;
                break;
            case cc.macro.KEY.e:
                // cc.log('golden ice');
                this.currentSelectObject = 11;
                break;
        }
    }
    onKeyUp(event){
        if(this.mouseX < 270 && this.mouseX > -430 && this.mouseY < 272 && this.mouseY > -272){
            switch(event.keyCode) {
                case cc.macro.KEY.a:
                    if(this.currentSelectObject == 1){
                        this.currentObjRef = new cc.Node("flame");
                        this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.flame;
                        this.currentObjRef.x = this.mouseX;
                        this.currentObjRef.y = this.mouseY;
                        this.currentObjRef.parent = this.node;
                    }
                    break;
                case cc.macro.KEY.s:
                    if(this.currentSelectObject == 2){
                        if(!this.characterCreateFlag){
                            this.characterCreateFlag = true;
                            
                            // this.currentObjRef = new cc.Node("character");
                            // this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.character;
                            // this.currentObjRef.x = this.mouseX;
                            // this.currentObjRef.y = this.mouseY;
                            // this.currentObjRef.parent = this.node;
                        }
                    }
                    break;
                case cc.macro.KEY.d:
                    if(this.currentSelectObject == 3){
                        this.currentObjRef = new cc.Node("obstacle");
                        this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.obstacle;
                        this.currentObjRef.x = this.mouseX;
                        this.currentObjRef.y = this.mouseY;
                        this.currentObjRef.parent = this.node;
                    }
                    break;
                case cc.macro.KEY.f:
                    if(this.currentSelectObject == 4){
                        this.currentObjRef = new cc.Node("ice");
                        this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.ice;
                        this.currentObjRef.x = this.mouseX;
                        this.currentObjRef.y = this.mouseY;
                        this.currentObjRef.parent = this.node;
                    }
                    break;
                case cc.macro.KEY.enter:
                    this.saveLastObjToLocalStorage();
                    cc.director.loadScene("Local Game");
                    break;
                case cc.macro.KEY.z:
                    //cc.log('up');
                    if(this.currentSelectObject == 5){
                        this.currentObjRef = new cc.Node("up");
                        this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.up;
                        this.currentObjRef.x = this.mouseX;
                        this.currentObjRef.y = this.mouseY;
                        this.currentObjRef.parent = this.node;
                    }

                    break;
                case cc.macro.KEY.x:
                    // cc.log('down');
                    if(this.currentSelectObject == 6){
                        this.currentObjRef = new cc.Node("down");
                        this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.down;
                        this.currentObjRef.x = this.mouseX;
                        this.currentObjRef.y = this.mouseY;
                        this.currentObjRef.parent = this.node;
                    } 
                    break;
                case cc.macro.KEY.c:
                    // cc.log('left');
                    if(this.currentSelectObject == 7){
                        this.currentObjRef = new cc.Node("left");
                        this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.left;
                        this.currentObjRef.x = this.mouseX;
                        this.currentObjRef.y = this.mouseY;
                        this.currentObjRef.parent = this.node;
                    }
                    break;
                case cc.macro.KEY.v:
                    // cc.log('right');
                    if(this.currentSelectObject == 8){
                        this.currentObjRef = new cc.Node("right");
                        this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.right;
                        this.currentObjRef.x = this.mouseX;
                        this.currentObjRef.y = this.mouseY;
                        this.currentObjRef.parent = this.node;
                    }
                    break;
                case cc.macro.KEY.q:
                    // cc.log('unlimited golden ice');
                    if(this.currentSelectObject == 9){
                        this.currentObjRef = new cc.Node("unlimitedGoldenIce");
                        this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.unlimitedGoldenIce;
                        this.currentObjRef.x = this.mouseX;
                        this.currentObjRef.y = this.mouseY;
                        this.currentObjRef.parent = this.node;
                    }
                    break;
                case cc.macro.KEY.w:
                    // cc.log('unlimited ice');
                    if(this.currentSelectObject == 10){
                        this.currentObjRef = new cc.Node("unlimitedIce");
                        this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.unlimitedIce;
                        this.currentObjRef.x = this.mouseX;
                        this.currentObjRef.y = this.mouseY;
                        this.currentObjRef.parent = this.node;
                    }
                    break;
                case cc.macro.KEY.e:
                    // cc.log('golden ice');
                    if(this.currentSelectObject == 11){
                        this.currentObjRef = new cc.Node("goldenIce");
                        this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.goldenIce;
                        this.currentObjRef.x = this.mouseX;
                        this.currentObjRef.y = this.mouseY;
                        this.currentObjRef.parent = this.node;
                    }
                    break;
            }
        }
        
    }
    toggleMusic(){
        if(cc.audioEngine.isMusicPlaying()){
            this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("MusicBtn")
            .getComponent(cc.Sprite).spriteFrame = this.bgmDisabled;
            cc.audioEngine.stopMusic();
        }
        else{
            this.node.getChildByName("Background").getChildByName("toolsbar").getChildByName("MusicBtn")
            .getComponent(cc.Sprite).spriteFrame = this.bgmAbled;
            cc.audioEngine.resumeMusic();
        }
    }
    onMouseDown(event){
       if(!this.isDrag && this.currentObjRef!=null){
           this.isDrag = true;
       }
    }

    onMouseUp(event){
        if(this.isDrag){
            this.isDrag = false;
        }
    }

    RecoverAllSpriteFrameFromLocalStorage(){
        //recover ice
        for(var i=0; i<iceListX.length; i++){
            this.currentObjRef = new cc.Node("ice");
            this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.ice;
            this.currentObjRef.x = iceListX[i];
            this.currentObjRef.y = iceListY[i];
            this.currentObjRef.parent = this.node;
        }
        //recover obstacle
        for(var i=0; i<obstacleListX.length; i++){
            this.currentObjRef = new cc.Node("obstacle");
            this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.obstacle;
            this.currentObjRef.x = obstacleListX[i];
            this.currentObjRef.y = obstacleListY[i];
            this.currentObjRef.parent = this.node;
        }
        //recover flame
        for(var i=0; i<flameListX.length; i++){
            this.currentObjRef = new cc.Node("flame");
            this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.flame;
            this.currentObjRef.x = flameListX[i];
            this.currentObjRef.y = flameListY[i];
            this.currentObjRef.parent = this.node;
        }
        //recover unlimited golden ice
        for(var i=0; i<unlimitedGoldenIceX.length; i++){
            this.currentObjRef = new cc.Node("unlimitedGoldenIce");
            this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.unlimitedGoldenIce;
            this.currentObjRef.x = unlimitedGoldenIceX[i];
            this.currentObjRef.y = unlimitedGoldenIceY[i];
            this.currentObjRef.parent = this.node;
        }
        //recover unlimited ice
        for(var i=0; i<unlimitedIceX.length; i++){
            this.currentObjRef = new cc.Node("unlimitedIce");
            this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.unlimitedIce;
            this.currentObjRef.x = unlimitedIceX[i];
            this.currentObjRef.y = unlimitedIceY[i];
            this.currentObjRef.parent = this.node;
        }
        //recover golden ice
        for(var i=0; i<goldenIceX.length; i++){
            this.currentObjRef = new cc.Node("goldenIce");
            this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.goldenIce;
            this.currentObjRef.x = goldenIceX[i];
            this.currentObjRef.y = goldenIceY[i];
            this.currentObjRef.parent = this.node;
        }
        //recover up
        for(var i=0; i<upX.length; i++){
            this.currentObjRef = new cc.Node("up");
            this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.up;
            this.currentObjRef.x = upX[i];
            this.currentObjRef.y = upY[i];
            this.currentObjRef.parent = this.node;
        }
        //recover down
        for(var i=0; i<downX.length; i++){
            this.currentObjRef = new cc.Node("down");
            this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.down;
            this.currentObjRef.x = downX[i];
            this.currentObjRef.y = downY[i];
            this.currentObjRef.parent = this.node;
        }
        //recover left
        for(var i=0; i<leftX.length; i++){
            this.currentObjRef = new cc.Node("left");
            this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.left;
            this.currentObjRef.x = leftX[i];
            this.currentObjRef.y = leftY[i];
            this.currentObjRef.parent = this.node;
        }
        //recover right
        for(var i=0; i<rightX.length; i++){
            this.currentObjRef = new cc.Node("right");
            this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.right;
            this.currentObjRef.x = rightX[i];
            this.currentObjRef.y = rightY[i];
            this.currentObjRef.parent = this.node;
        }
        //recover character
        this.currentObjRef = new cc.Node("character");
        this.currentObjRef.scaleY = 0.653;
        this.currentObjRef.addComponent(cc.Sprite).spriteFrame = this.character;
        this.currentObjRef.x = characterX;
        this.currentObjRef.y = characterY;
        this.currentObjRef.parent = this.node;
    }

    toggleBoard(event){
        
        if(!this.board.active){
            this.board.active = true;
        }
        else{
            this.board.active = false;
        }
        
    }

    startLocalGame(){
        this.saveLastObjToLocalStorage();
        cc.director.loadScene("Local Game");
    }

    uploadToFirebase(){
        var db = firebase.database();
        var currentStage = 0;
        var totalStage = 0;
        db.ref('totalStage').once('value', function(snapshot){
            totalStage = snapshot.val();
            //cc.log("totalStage " + totalStage);
        }).then(function(){
            totalStage += 1;
            currentStage = totalStage;
            db.ref('/').update({
                totalStage: totalStage
            })
        }).then(function(){
            //create new stage space
            db.ref('/'+currentStage).set({
                author: authorName,
                characterX: characterX,
                characterY: characterY,
                flameListX: 0,
                flameListY: 0,
                obstacleListX: 0,
                obstacleListY: 0,
                iceListX: 0,
                iceListY: 0,
                unlimitedGoldenIceX: 0,
                unlimitedGoldenIceY: 0,
                unlimitedIceX: 0,
                unlimitedIceY: 0,
                goldenIceX: 0,
                goldenIceY: 0,
                upX: 0,
                upY: 0,
                downX: 0,
                downY: 0,
                leftX: 0,
                leftY: 0,
                rightX: 0,
                rightY: 0
            })
        }).then(function(){
            for(var i=0; i<flameListX.length; i++){
                db.ref('/'+currentStage+'/flameListX').push({
                    flameListX: flameListX[i]
                })
                db.ref('/'+currentStage+'/flameListY').push({
                    flameListY: flameListY[i]
                })
            }
            for(var i=0; i<obstacleListX.length; i++){
                db.ref('/'+currentStage+'/obstacleListX').push({
                    obstacleListX: obstacleListX[i]
                })
                db.ref('/'+currentStage+'/obstacleListY').push({
                    obstacleListY: obstacleListY[i]
                })
            }
            for(var i=0; i<iceListX.length; i++){
                db.ref('/'+currentStage+'/iceListX').push({
                    iceListX: iceListX[i]
                })
                db.ref('/'+currentStage+'/iceListY').push({
                    iceListY: iceListY[i]
                })
            }
            for(var i=0; i<unlimitedGoldenIceX.length; i++){
                db.ref('/'+currentStage+'/unlimitedGoldenIceX').push({
                    unlimitedGoldenIceX: unlimitedGoldenIceX[i]
                })
                db.ref('/'+currentStage+'/unlimitedGoldenIceY').push({
                    unlimitedGoldenIceY: unlimitedGoldenIceY[i]
                })
            }
            for(var i=0; i<unlimitedIceX.length; i++){
                db.ref('/'+currentStage+'/unlimitedIceX').push({
                    unlimitedIceX: unlimitedIceX[i]
                })
                db.ref('/'+currentStage+'/unlimitedIceY').push({
                    unlimitedIceY: unlimitedIceY[i]
                })
            }
            for(var i=0; i<goldenIceX.length; i++){
                db.ref('/'+currentStage+'/goldenIceX').push({
                    goldenIceX: goldenIceX[i]
                })
                db.ref('/'+currentStage+'/goldenIceY').push({
                    goldenIceY: goldenIceY[i]
                })
            }
            for(var i=0; i<upX.length; i++){
                db.ref('/'+currentStage+'/upX').push({
                    upX: upX[i]
                })
                db.ref('/'+currentStage+'/upY').push({
                    upY: upY[i]
                })
            }
            for(var i=0; i<downX.length; i++){
                db.ref('/'+currentStage+'/downX').push({
                    downX: downX[i]
                })
                db.ref('/'+currentStage+'/downY').push({
                    downY: downY[i]
                })
            }
            for(var i=0; i<leftX.length; i++){
                db.ref('/'+currentStage+'/leftX').push({
                    leftX: leftX[i]
                })
                db.ref('/'+currentStage+'/leftY').push({
                    leftY: leftY[i]
                })
            }
            for(var i=0; i<rightX.length; i++){
                db.ref('/'+currentStage+'/rightX').push({
                    rightX: rightX[i]
                })
                db.ref('/'+currentStage+'/rightY').push({
                    rightY: rightY[i]
                })
            }
        })
        
    }

    resetLocalStorage(){
        characterX = -64;
        characterY = 0;
        flameListX = [];
        flameListY = [];
        obstacleListX = [];
        obstacleListY = [];
        iceListX = [];
        iceListY = [];
        unlimitedGoldenIceX = [];
        unlimitedGoldenIceY = [];
        unlimitedIceX = [];
        unlimitedIceY = [];
        goldenIceX = [];
        goldenIceY = [];
        upX = [];
        upY = [];
        downX = [];
        downY = [];
        leftX = [];
        leftY = [];
        rightX = [];
        rightY = [];
        
    }

    returnToMenu(){
        cc.director.loadScene("Menu");
    }

    saveLastObjToLocalStorage(){
        switch(this.currentSelectObject){
            case 1:   
                flameListX.push(this.currentObjRef.x);
                flameListY.push(this.currentObjRef.y);
                break;
            case 2:
                characterX = this.currentObjRef.x;
                characterY = this.currentObjRef.y;
                break;
            case 3:
                obstacleListX.push(this.currentObjRef.x);
                obstacleListY.push(this.currentObjRef.y);
                break;
            case 4:
                iceListX.push(this.currentObjRef.x);
                iceListY.push(this.currentObjRef.y);
                break;
            case 5:
                upX.push(this.currentObjRef.x);
                upY.push(this.currentObjRef.y);
                break;
            case 6:
                downX.push(this.currentObjRef.x);
                downY.push(this.currentObjRef.y);
                break;
            case 7:
                leftX.push(this.currentObjRef.x);
                leftY.push(this.currentObjRef.y);
                break;
            case 8:
                rightX.push(this.currentObjRef.x);
                rightY.push(this.currentObjRef.y);
                break;
            case 9:
                unlimitedGoldenIceX.push(this.currentObjRef.x);
                unlimitedGoldenIceY.push(this.currentObjRef.y);
                break;
            case 10:
                unlimitedIceX.push(this.currentObjRef.x);
                unlimitedIceY.push(this.currentObjRef.y);
                break;
            case 11:
                goldenIceX.push(this.currentObjRef.x);
                goldenIceY.push(this.currentObjRef.y);
                break;
        }
    }
}
